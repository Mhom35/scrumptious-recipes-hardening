from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.decorators.http import require_http_methods
from django.db import IntegrityError

from recipes.forms import RatingForm

# from recipes.forms import RecipeForms
from recipes.models import Recipe, Ingredient, ShoppingItem


def log_rating(request, recipe_id):
    try:
        if request.method == "POST":
            form = RatingForm(request.POST)
            if form.is_valid():
                rating = form.save(commit=False)
                rating.recipe = Recipe.objects.get(pk=recipe_id)
                rating.save()
        return redirect("recipe_detail", pk=recipe_id)
    except Recipe.DoesNotExist:
        return redirect("recipes_list")


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 2


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()

        # Create a new empty list and assign it to a variable
        foods = []

        # For each item in the user's shopping items, which we
        # can access through the code
        # self.request.user.shopping_items.all()
        for item in self.request.user.shoppinglist.all():
            # Add the shopping item's food to the list
            foods.append(item.food_item)

        # Put that list into the context
        context["food_in_shopping_list"] = foods

        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "description", "image"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "author", "description", "image"]
    success_url = reverse_lazy("recipes_list")


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


class ShoppingItemListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "shopping_list/list.html"

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)

@require_http_methods(["POST"])
def create_shopping_item(request):
    # Get the value for the "ingredient_id" from the
    ingredient_id = request.POST.get("ingredient_id")
    # request.POST dictionary using the "get" method

    # Get the specific ingredient from the Ingredient model
    # using the code
    ingredient = Ingredient.objects.get(id=ingredient_id)
    # Ingredient.objects.get(id=the value from the dictionary)

    # Get the current user which is stored in request.user
    user = request.user

    try:
        # Create the new shopping item in the database
        ShoppingItem.objects.create(
            food_item=ingredient.food,
            user=user,
        )
    except IntegrityError:
        pass

    return redirect("recipe_detail", pk=ingredient.recipe.id)


@require_http_methods(["POST"])
def delete_all_shopping_item(request):
    ShoppingItem.objects.filter(user=request.user).delete()
    return redirect("shopping_item_list")
