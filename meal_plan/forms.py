from django import forms
from django.forms import ModelForm

from .models import MealPlan


class DateInput(forms.DateInput):
    input_type = "date"


class MealPlanForm(ModelForm):
    class Meta:
        model = MealPlan
        fields = ["name", "owner", "date", "recipes"]
        widgets = {
            "date": DateInput(),
        }
