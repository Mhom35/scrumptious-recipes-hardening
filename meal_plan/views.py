from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from meal_plan.forms import MealPlanForm
from meal_plan.models import MealPlan

# Create your views here.


class MealPlanListView(ListView):
    model = MealPlan
    template_name = "meal_plan/list.html"
    # paginate_by = 2


class MealPlanCreateView(CreateView):
    model = MealPlan
    template_name = "meal_plan/create.html"
    # fields = ["name", "recipes", "date"]
    # success_url = reverse_lazy("meal_plan_detail")
    form_class = MealPlanForm

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse_lazy("meal_plan_detail", args=[self.object.id])


class MealPlanDetailView(DetailView):
    model = MealPlan
    template_name = "meal_plan/detail.html"


class MealPlanUpdateView(LoginRequiredMixin, UpdateView):
    model = MealPlan
    template_name = "meal_plan/edit.html"
    fields = ["name", "date", "recipes"]
    success_url = reverse_lazy("meal_plan_list")


class MealPlanDeleteView(LoginRequiredMixin, DeleteView):
    model = MealPlan
    template_name = "meal_plan/delete.html"
    success_url = reverse_lazy("meal_plan_list")


# class PromiseCreateView(CreateView):
#     model = Promise
#     form_class = PromiseForm
